# README #

### What is this repository for? ###

A simple express server to practice with devops and devsecops pipelines.

* Version 0.0.0
* Our design choices for the app can be found in the [wiki](https://bitbucket.org/tymyrddin/express-base/wiki/).

### How do I get set up? ###

After cloning this repo, install dependencies:


```shell
$ npm install
```

To run the app under development with `nodemon`:

```
$ npm devstart
```

View the server in your browser at http://127.0.0.1:3000/

